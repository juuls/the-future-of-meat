module.exports = function(grunt) {

	grunt.initConfig({
		// Livereload & sass logic
		watch: {
			sass: {
				files: ['<%= sass.dev.src %>'],
				tasks: 'sass:dev',
				options: {
					livereload: true
				}
			},

			html: {
				files: ['*.html'],
				options: {
					livereload: true
				}
			},

			javascript: {
				files: ['javascripts/**/*.js'],
				options: {
					livereload: true
				}
			}
		},

		// Copy stuff from bower_components to live
		copy: {
			move: {
				src: 'bower_components/move.js/move.min.js',
				dest: 'javascripts/move.min.js'
			},

			jquery: {
				src: 'bower_components/jquery/dist/jquery.min.js',
				dest: 'javascripts/jquery.min.js'
			}
		},

		// Compile sass with foundation apps in the path
		sass: {
			options: {
				includePaths: ['bower_components/foundation/scss']
			},

			// Create files array dynamically for use in contrib-watch
			dev: {
				expand: true,
				flatten: true,
				src: ['scss/*.scss'],
				dest: 'stylesheets/',
				ext: '.css',
				options: {
					style: 'expanded'
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-sass');

	grunt.registerTask('default', ['copy', 'watch']);
}
